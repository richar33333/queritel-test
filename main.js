let UserList = []
function getElement(e) {
  e.preventDefault();
  let ipAdrress = document.getElementById('ipAdrress').value
  console.log()
  let url = `https://api.queritel.com/api/test-lab/hernandez/query_ip.php?ip=${ipAdrress}`
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  fetch(proxyurl + url) // https://cors-anywhere.herokuapp.com/https://example.com
  .then(response => response.json())
  .then((contents) => {
    console.log(contents)
    let html = `
    <div class="card border-secondary mt-4">
      <div class="card-body text-center">
        <h4 class="card-title">${contents.ip_address}</h4>
        <img src="${contents.country_flag}" alt="" width="180">
        <p class="card-text">${contents.country_name}</p>
        <p class="card-text">${contents.country_code}</p>
      </div>
    </div>
    `

    document.getElementById('countryResponse').innerHTML = html
    getUser()
  })
  .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))
}

async function getUser() {
  let url = 'https://api.queritel.com/api/test-lab/hernandez/get_user_list.php'
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  await fetch(proxyurl + url) // https://cors-anywhere.herokuapp.com/https://example.com
  .then(response => response.json())
  .then((userList) => {
    usersList = userList
    html = ''
    html += `<ul class="list-group">`
    userList.forEach(user => {
      html += `
          <li class="list-group-item listItem" id="${user.display_name}">${user.first_name} ${user.last_name} </li>
          `
    });
    html += `</ul>`
    setTimeout(() => {
      getUserList()
      
    }, 1000);
    document.getElementById('userList').innerHTML = html
  })
  .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))
}

function getUserList() {
  const list = document.querySelectorAll('.listItem')
  list.forEach(element => {
    element.addEventListener('click', function (e) {
      getuserDetail(e.target.id)
    })
  });
}

async function getuserDetail(id) {
  let url = `https://api.queritel.com/api/test-lab/hernandez/get_user_detail.php?display_name=${id}`
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  await fetch(proxyurl + url) // https://cors-anywhere.herokuapp.com/https://example.com
  .then(response => response.json())
  .then((userDetail) => {
    console.log(userDetail)

    html = ''
    html += `<ul class="list-group">`
    html += `
        <li class="list-group-item listItem" id="${userDetail.display_name}">${userDetail.first_name}  </li>
        <li class="list-group-item listItem">${userDetail.last_name}  </li>
        <li class="list-group-item listItem">${userDetail.display_name}  </li>
        <li class="list-group-item listItem">${userDetail.country_name}  </li>
        <li class="list-group-item listItem">${userDetail.country_code}  </li>
        <li class="list-group-item listItem"><img src="${userDetail.country_flag}" alt="" width="180">  </li>
        <li class="list-group-item listItem">${userDetail.ip_address}  </li>
        `

    html += `</ul>`
    document.getElementById('userDetail').innerHTML = html
  })
  .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))
}
